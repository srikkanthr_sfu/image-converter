cmake_minimum_required(VERSION 2.8.11)
project(FileConverter)

set(NIFTI_SRC ${PROJECT_SOURCE_DIR}/niftilib/NIFTI)
set(NIFTI_INCS	${NIFTI_SRC}/include)
set(ETWOE_INCS	${PROJECT_SOURCE_DIR}/src)
set(ZNZ_INCS	/usr/include)

set(NIFTI_LIBS	${NIFTI_SRC}/lib)
set(ZNZ_LIBS	/usr/lib)

find_package(OpenJPEG REQUIRED)
include_directories(${OPENJPEG_INCLUDE_DIRS} ${PROJECT_SOURCE_DIR}/src ${NIFTI_INCS} ${ETWOE_INCS} ${ZNZ_INCS})
link_directories(${NIFTI_LIBS} ${ZNZ_LIBS})
add_executable(conv src/main.cpp)
target_link_libraries(conv ${OPENJPEG_LIBRARIES} fslio niftiio znz m z)
