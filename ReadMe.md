# Command line tool to convert Topcon (.fda, .fds) and Heidelberg (.e2e) files


##                 Pre-requisites
Ensure that the following are installed before attempting to build this project

1. make - On debian (includes ubuntu) based systems, it can be installed with 

    ```
    sudo apt-add-repository ppa:ubuntu-desktop/ubuntu-make
    sudo apt-get update
    sudo apt-get install ubuntu-make
    ```

2. cmake - On debian (includes ubuntu) based systems, it can be installed with

    ```
    sudo apt-get update
    sudo apt-get -y install cmake
    ```

3. OpenJPEG - Detailed instructions available [here](https://github.com/uclouvain/openjpeg/blob/master/INSTALL.md)

    For Debian based systems the following should work

    Install dependencies of OpenJPEG

    ```
        sudo apt-get install liblcms2-dev  libtiff-dev libpng-dev libz-dev
    ```

    Clone the library from github

    ```
        git clone https://github.com/uclouvain/openjpeg.git
    ```

    Navigate to the folder in command line and execute the following

    ```
        mkdir build
        cd build
        cmake .. -DCMAKE_BUILD_TYPE=Release
        make
    ```

4. NIFTI library - Clone the library

    ```
    git clone https://github.com/MIRTK/NIFTI.git
    ```

    Run cmake and make in the NIFTI foler

    ```
    cmake .
    make
    ```

##                  Building the tool

1. Open CMakeLists.txt in the project folder and replace NIFTI_SRC variable with location of NIFTI in local folder

2. Create a folder called "build" in this folder 

    ```
        mkdir build
    ```

3. Navigate to build folder

    ```
        cd build
    ```

4. Run CMakeLists.txt present in the previous folder

    ```
        cmake ..
    ```

5. Run make (created by cmake in the current folder)

    ```
        make
    ```

6. Built binary "conv" is left in the build folder

##                       How to use
1. Template -

    ```
        <Project_src_location>/build/conv <Source_file_address> <options>
    ```

       Source file must be a valid ".e2e", ".E2E", ".fda" or ".fds" file
    
       <options> can include : 
        
       
            1. "--nii" or "-n" followed by destination address with required filename (filename must end with ".nii")
        
            2. "--patientinfo" or "-p" followed by destination address of txt file with required filename
        
            3. "--fundus" of "-f" followed by destination address with required filename 
        
            4. "--dbg" to view prompts at the command line, used for troubleshooting
        