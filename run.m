clear

file = fopen('/home/sramacha/Documents/image-converter/build/myfundus.txt', 'r');

%% Read 3D image from txt file

spec = '%f';
raw = fscanf(file, spec);

offset = 0;
slice = 1;
while offset < length(raw)
    w = raw(offset + 1, 1);
    h = raw(offset + 2, 1);

    img = zeros(w, h);
    offset = offset + 2;
    for i = 1:w
        for j = 1:h
            img(i, j) = raw((i-1) * h + j + offset, 1);
        end
    end
    offset = offset + h * w;
    img3(:, :, slice) = img;
    slice = slice + 1;
end
imshow3D(img3, []);
