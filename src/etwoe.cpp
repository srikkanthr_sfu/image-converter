#pragma once


#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <math.h>
#include <assert.h>

using namespace std;


namespace etwoe {

	/** Custom ufloat structure, used in e2e formats to represent 3D image
		m -> first 10 bits
		e -> next 6 bits
		number representation = 1.xyz * 2^(e-63) when 0 < e < 63
		                      = 0.xyz * 2^(-62)  when e = 0
		                      = 0                when e = 63
		                      where xyz = decimals obtained from m / 1024
	*/
	struct ufloat16 {
    	uint16_t m:10;
    	uint16_t e:6;

    	operator float() const {
      	if (e == 0)
        	return ldexp(m / float(1<<10), -62);
      	else if (e == 63)
        	return 0.0f;
      	else
        	return ldexp(1.0f + m / float(1<<10), int(e)-63);
    	}
  	};

	void ignoreUntil(string s, fstream &f) {
		int i = 0;
		while(i < s.length()) {
			char *now = new char[1];
			f.read((char*)now, sizeof(char));
			if (s[i] == *now) i++;
			else i = 0;
		}
	}

	struct main_header {
		char magic[8];
		uint32_t vers;
		uint16_t garbage[10];
		bool check() {
			for (int i=0;i<9;i++) if (garbage[i] != 0xffff) return false;
			if (garbage[9] != 0) return false;  
			if (vers != 0x64) return false;
			return true;
		}
	};

	struct header {
		char magic[12];
		uint32_t vers;
		uint16_t garbage[10];
		uint32_t num, curr, zero, x;
		bool check() {
			for (int i=0;i<9;i++) if (garbage[i] != 0xffff) return false;
			if (garbage[9] != 0) return false;  
			if (vers != 0x64) return false;
			return true;
		}
	};

	struct chunk_data {
	   	uint32_t pos, start, size, zero_1, patient_id, study_id, series_id, slice_id;
	   	uint16_t e, zero_2;
	   	uint32_t tag, id;
	   	bool check() {
	   		if (zero_1 != 0 || zero_2 != 0) return false;
	   		return true;
	   	}
	};

	struct dir_entry {
	  	char magic[12];
	  	uint32_t vers;
	  	uint16_t minus1[9], zero_1;
	  	uint32_t count, cur, prev, id;
	  	bool check() {
	  		if (vers != 0x64) return false;
	  		if (zero_1) return false;
	  		for (int i=0;i<9;i++) if (minus1[i] != 0xffff) return false; 
	  		return true;
	  	}
	};

	struct chunk_header {
    	char magic[12];
    	uint32_t a, b, pos, size, zero, patient_id, study_id, series_id, slice_id;
    	uint16_t ind, f;
    	uint32_t tag, id;
    	bool check() {
    		if (zero) return false;
    		return true;
    	}
  	}; 

  	struct patient_info {
  		char first_name[62], last_name[132];
  		//birthdate
  		int year, month, day;
  		char sex;
  		char laterality[28];
  	};

	class File {
	public:
		main_header mainh_t;
		header dir_header;
		vector<chunk_data> chunk_meta;
		vector<chunk_header> chunk_meta2;
		vector<vector<uint8_t>> fundus;
		vector<vector<float>> contour;
		vector<vector<vector<double>>> tomogram;
		patient_info pinfo;

		void PutTomogramRaw(string s) {
			if (tomogram.empty() || tomogram[0].empty() || tomogram[0][0].empty()) return;
			fstream f(s.c_str(), ios::out | ios::binary);
			int slices = tomogram.size(), height = tomogram[0].size(), width = tomogram[0][0].size();
			f.write((char*)&slices, sizeof(slices));
			f.write((char*)&height, sizeof(height));
			f.write((char*)&width, sizeof(width));
			char *ch = new char[sizeof(double) * width * height];
			for (int i=0;i<slices;i++) {
				for (int j=0;j<height;j++) for (int k=0;k<width;k++) {
					ch[j*width+k] = tomogram[i][j][k];
				}
				f.write((char*)ch, sizeof(double) * width * height);
			}
			f.close();
		}
		void PutFundusRaw(string s) {
			int m = fundus.size(), n, i;
			if (!fundus.empty()) n = fundus[0].size();
			else {
				cerr << "No fundus found";
				return;
			}
			fstream f(s.c_str(), ios::out);
			f << m << " " << n << '\n';
			// cout << m << " " << n << '\n';
			char *ch = new char[sizeof(uint8_t) * m * n];
			for (int i=0;i<m;i++) for (int j=0;j<n;j++) {
				f << (int)fundus[i][j] << " \n"[j==n-1];
			}
			f.close();
		}
	};

	void read(string s, File &file) {
		fstream f(s.c_str(), ios::in | ios::binary);
		if (!f.is_open()) throw(runtime_error("Error reading file, does not exist\n"));
		ignoreUntil("CMDb", f);
		f.read((char*)&file.mainh_t, sizeof(file.mainh_t));
		if (!file.mainh_t.check()) throw(runtime_error("Error reading main header\n"));

		f.read((char*)&file.dir_header, sizeof(file.dir_header));
		if (!file.dir_header.check()) throw(runtime_error("Error reading directory header\n"));

		uint32_t curr = file.dir_header.curr;
		do {
			dir_entry ch_t;
			f.seekg(curr, ios::beg);
			f.read((char*)&ch_t, sizeof(ch_t));
			if (!ch_t.check()) throw(runtime_error("Error reading dir_entry\n"));
			for (int i=0;i<ch_t.count;i++) {
				chunk_data ht;
				f.read((char*)&ht, sizeof(ht));
				file.chunk_meta.push_back(ht);
				if (!ht.check()) throw(runtime_error("Error reading chunk data\n"));
			}
			curr = ch_t.prev;
		} while(curr);

		int cnt = 0;
		for (auto &it : file.chunk_meta) if (it.start > it.pos) {
			chunk_header chd_t;
			f.seekg(it.start);
			f.read((char*)&chd_t, sizeof(chd_t));
			if (!chd_t.check()) throw(runtime_error("Error reading chunk header\n"));
			if (chd_t.tag == 0x40000000) {

				uint32_t sz, x, y, height, width;
				f.read((char*)&sz, sizeof(sz));
				f.read((char*)&x, sizeof(x));
				f.read((char*)&y, sizeof(y));
				f.read((char*)&height, sizeof(height));
				f.read((char*)&width, sizeof(width));


				if (chd_t.ind == 0) { //fundus
					uint8_t *ptr = new uint8_t[width * height];
					// cout << width << " " << height << " w\n";
					f.read((char*)ptr, sizeof(uint8_t) * width * height);
					for (int i=0;i<height;i++) {
						vector<uint8_t> now(width);
						for (int j=0;j<width;j++) {
							now[j] = ptr[i * width + j];
						}
						file.fundus.push_back(now);
					}
					// cout << file.fundus.size() << " " << file.fundus[0].size() << '\n';
				} else { //tomogram

					ufloat16 *ptr = new ufloat16[width * height];
					if (!f.read((char*)ptr, sizeof(ufloat16) * width * height)) throw runtime_error("Error reading tomogram image\n");
					vector<vector<double>> pout(height, vector<double>(width, 0));
					for (int y=0;y<height;y++) for (int x=0;x<width;x++) {
						pout[y][x] =  256 * pow(ptr[y*width+x], 1.0f / 2.4f);
					}
					file.tomogram.push_back(pout);

				}
			} else if (chd_t.tag == 0x00002723) { //contour data

				uint32_t x, name, y, width;
				f.read((char*)&x, sizeof(x));
				f.read((char*)&name, sizeof(name));
				f.read((char*)&y, sizeof(y));
				f.read((char*)&width, sizeof(width));
				float *ptr = new float[width];
				f.read((char*)ptr, sizeof(float) * width);
				vector<float> row(width);
				for (int i=0;i<width;i++) row[i] = ptr[i];
				file.contour.push_back(row);

			} else if (chd_t.tag == 0x00000009) {

				char s[67], t[67];
				f.read((char*)s, sizeof(char) * 31);
				char *q = file.pinfo.first_name;
				for (int i=0;i<31;i++) {
					if (s[i] < 128) *q++ = s[i];
					else {
						*q++ = 0xc2 + (s[i]>0xbf);
						*q++ = 0x80 + (s[i]&0x3f);
					}
				}
				*q = '\0';
				f.read((char*)s, sizeof(char) * 66);
				q = file.pinfo.last_name;
				for (int i=0;i<31;i++) {
					if (s[i] < 128) *q++ = s[i];
					else {
						*q++ = 0xc2 + (s[i]>0xbf);
						*q++ = 0x80 + (s[i]&0x3f);
					}
				}
				*q = '\0';

				uint32_t J;
				f.read((char*)&J, sizeof(J));
				J = J/64 - 14558805;
				int f2 = J + 1401 + (((4 * J + 274277) / 146097) * 3) / 4 - 38;
    			int e = 4 * f2 + 3;
    			int g = (e % 1461) / 4;
    			int h = 5 * g + 2;
    			file.pinfo.day = (h % 153) / 5 + 1;
    			file.pinfo.month = (h / 153 + 2) % 12 + 1;
    			file.pinfo.year = (e / 1461) - 4716 + (12 + 2 - file.pinfo.month) / 12;

				f.read((char*)&file.pinfo.sex, sizeof(char));

			} else if (chd_t.tag == 0x0000000B) {

				char s[14];
				f.read((char*)s, sizeof(char) * 14);
				f.read((char*)s, sizeof(char));
				file.pinfo.laterality[0] = s[0];
				file.pinfo.laterality[1] = '\0';
				// cout << file.pinfo.laterality << " laterality\n";

			} else { //other tag's are not maintained

			}
		}
		// cout << file.contour.size() << '\n';
		cerr << "Extracted Image successfully\n";
		f.close();
	}

	void OutputPatientInfo(string s, File &file) {
		fstream f(s.c_str(), ios::out);
		f << "name : ";
		f << file.pinfo.first_name << ", " << file.pinfo.last_name << '\n';
		f << "birth date : ";
		f << file.pinfo.day << "/" << file.pinfo.month << "/" << file.pinfo.year << '\n';
		f << "sex : ";
		f << file.pinfo.sex << '\n';
		f << "laterality : ";
		f << file.pinfo.laterality << '\n';
		f.close();
	}
}
