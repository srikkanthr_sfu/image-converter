#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>
#include <j2kconv.hpp>
using namespace std;

namespace fda {
  class File;
  fstream f;

  struct getinput {
    char foct[4];
    char fda[3];
    uint32_t a[1];
  };

  struct chunk {
    uint8_t name_sz;
    string name;
    uint32_t data_sz;
    vector<uint8_t> data;
    chunk() {}
    void Get(File *file);
    void Process(File *file);
  };

  uint32_t getnxt32(vector<uint8_t> &data, int &it) {
    uint8_t conv[4];
    conv[0] = data[it];
    conv[1] = data[it+1];
    conv[2] = data[it+2];
    conv[3] = data[it+3];
    it += 4;
    return *((uint32_t*)conv);
  }

  uint16_t getnxt16(vector<uint8_t> &data, int &it) {
    uint8_t conv[2];
    conv[0] = data[it];
    conv[1] = data[it+1];
    it += 2;
    return *((uint16_t*)conv);
  }

  struct TopconImage {
    uint8_t type;
    uint32_t width, height, num_slices, check;
    vector<vector<uint8_t>> encoded_image;
    TopconImage() {}
    void setimage(vector<uint8_t> &data) {
      int n = data.size(), i;
      type = data[0];
      i = 9;
      width = getnxt32(data, i);
      height = getnxt32(data, i);
      num_slices = getnxt32(data, i);
      check = getnxt32(data, i);
      for (int newj=0;newj<num_slices;newj++) {
        int32_t sz; uint8_t conv[4];
        conv[0] = data[i];i++;
        conv[1] = data[i];i++;
        conv[2] = data[i];i++;
        conv[3] = data[i];i++;
        sz = *((int32_t*)conv);
        vector<uint8_t> cur_slice;
        for (int j=0;j<sz;j++) {
          cur_slice.push_back(data[i]);
          i++;
        }
        encoded_image.push_back(cur_slice);
      }
    }
    void put(string dest_file) {
      cerr << "Writing Topcon Raw Image to file\n";
      fstream fout(dest_file.c_str(), ios::out | ios::binary);
      fout.write((char*)&width, sizeof(width));
      fout.write((char*)&height, sizeof(height));
      fout.write((char*)&num_slices, sizeof(num_slices));
      for (int i=0;i<num_slices;i++) {
        uint32_t sz = encoded_image[i].size();
        fout.write((char*)&sz, sizeof(sz));
        for (auto it : encoded_image[i]) fout.write((char*)&it, sizeof(it));
      }
      fout.close();
      cerr << "Image successfully extracted\n";
    }
  };

  struct PInfo {
    char model_name[16], serial_no[16], version[16];
    uint16_t year, month, day, hour, minute, second;
    void set_hw_info(vector<uint8_t> &data) {
      int i = 0;
      for (int j=0;j<16;j++) {model_name[j] = data[i];i++;}
      for (int j=0;j<16;j++) {serial_no[j] = data[i];i++;}
      i += 32;
      for (int j=0;j<16;j++) {version[j] = data[i];i++;}
      year = getnxt16(data, i);
      month = getnxt16(data, i);
      day = getnxt16(data, i);
      hour = getnxt16(data, i);
      minute = getnxt16(data, i);
      second = getnxt16(data, i);
      i += 8;
    }
  };
  template<typename T>
  void ReadType(T *&inptr, int m) {
    inptr = new T [m];
    if (!f.read((char*)inptr, sizeof(T) * m)) {
      cerr << "Error in ReadType(), file not read properly\n";
      cerr << m << " multiple data requested, possibly not present\n";
      exit(1);
    }
    // return *inptr;
  }

  template<typename T>
  T GetType() {
    T *res = new T[1];
    if (!f.read((char*)res, sizeof(T))) {
      cerr << "Error in GetType(), file not read properly\n";
      exit(1);
    }
    return *res;
  }
  class File {
  public:

    /*---------- Extracted 3-D image -------------*/
    int width, height, slices;
    vector<vector<vector<uint8_t>>> img;

    int f_width, f_height, f_channels;
    vector<vector<vector<uint8_t>>> fundus_img;

    bool ImagePresent;
    /*--------------------------------------------*/

    /*----------- Patient Info (General stuff like name, dates etc.) ------------------- */
    PInfo p;
    /*---------------------------------------------------------------------------------- */

    /*------------- Raw chunks stored for the purpose of read/write -------------------- */
    vector<chunk> list_chunks;
    /*---------------------------------------------------------------------------------- */


    /*----------------------- Extract info from read ----------------------------------- */
    void read(string src) {
      f.open(src.c_str(), ios::in | ios::binary);
      if (!f.is_open()) throw(runtime_error("Error reading file, does not exist\n"));
      char *a;
      uint32_t *b;
      ReadType<char>(a, 7);
      ReadType<uint32_t>(b, 2);
      while(true) {
        chunk new_chunk;
        new_chunk.Get(this);
        if (new_chunk.name_sz == 0) break;
        cerr << new_chunk.name << " ";
        cerr << new_chunk.data_sz << " bytes\n";
        list_chunks.push_back(new_chunk);
      }
      cerr << "File successfully read\n";
      f.close();
    }
    /*----------------------------------------------------------------------------------- */

    void PutFundusRaw(string dest_file) {
      fstream f(dest_file.c_str(), ios::out);
      for (int i=0;i<f_channels;i++) {
        f << f_width << " " << f_height << '\n';
        for (int j=0;j<f_width;j++) for (int k=0;k<f_height;k++) {
          f << (int)fundus_img[i][j][k] << " \n"[k==f_height-1];
        }
      }
    }
  };


  void chunk::Get(File *file) {
    name_sz = GetType<uint8_t>();
    if (name_sz == 0) return;

    name.resize(name_sz);
    for (auto &it : name) it = GetType<uint8_t>();
    data_sz = GetType<uint32_t>();
    data.resize(data_sz);
    for (auto &it : data) {
      it = GetType<uint8_t>();
    }
    Process(file);
  }

  void chunk::Process(File *file) {
    if (name == "@IMG_JPEG") {
      TopconImage Tim;
      Tim.setimage(data);
      int width = Tim.width;
      int height = Tim.height;
      int slices = Tim.num_slices;

      file->width = width;
      file->height = height;
      file->slices = slices;

      file->img.resize(slices, vector<vector<uint8_t>>(width, vector<uint8_t>(height, 0)));

      for (int sl = 0; sl < slices; ++sl) {
        int sz = Tim.encoded_image[sl].size();

        char *buf = new char[sz];
        for (int j=0;j<sz;j++) buf[j] = Tim.encoded_image[sl][j];
        auto go = decode_j2k(buf, sz);
        for (int i=0;i<width;i++) {
          for (int j=0;j<height;j++) {
            file->img[sl][i][j] = go[0][i][j];
          }
        }
      }
      cerr << "Image successfully read\n";
    } else if (name == "@HW_INFO_03") {
      file->p.set_hw_info(data);
    } else if (name == "@ALIGN_INFO") {

    } else if (name == "@ANTERIOR_CALIB_INFO") {

    } else if (name == "@CAPTURE_INFO_02") {

    } else if (name == "@CONTOUR_INFO") {

    } else if (name == "@EFFECTIVE_SCAN_RANGE") {

    } else if (name == "@FAST_Q2_INFO") {

    } else if (name == "@FDA_FILE_INFO") {

    } else if (name == "@GLA_LITTMANN_01") {

    } else if (name == "@IMG_FUNDUS") {
      uint32_t width, height, depth, x, bpp;
      int it = 0;
      char *s = new char[4];
      width = getnxt32(data, it);
      height = getnxt32(data, it);
      bpp = getnxt32(data, it);
      depth = getnxt32(data, it);
      for (int i=0;i<4;i++) s[i] = data[it++];
      if (depth != 1) throw runtime_error("depth != 1 in Fundus\n");
      uint32_t channels = bpp / 8, sz;

      file->f_width = width;
      file->f_height = height;
      file->f_channels = channels;
      file->fundus_img.resize(channels, vector<vector<uint8_t>>(width, vector<uint8_t>(height, 0)));

      sz = getnxt32(data, it);
      
      char *buf = new char[sz];
      for (int i=0;i<sz;i++) buf[i] = data[it++];
      auto go = decode_j2k(buf, sz);
      assert(go.size() == channels && go[0].size() == width && go[0][0].size() == height);
      for (int i=0;i<channels;++i)
        for (int j=0;j<width;++j)
          for (int k=0;k<height;++k) {
            // file->fundus_img[i][j][k] = go[2-i][j][k];
            file->fundus_img[i][j][k] = go[i][j][k];
          }

    } else if (name == "@IMG_MOT_COMP_03") {

    } else if (name == "@IMG_PROJECTION") {

    } else if (name == "@IMG_TRC_02") {

    } else if (name == "@PARAM_OBS_02") {

    } else if (name == "@PARAM_SCAN_04") {

    } else if (name == "@PARAM_TRC_02") {

    } else if (name == "@PATIENTEXT_INFO") {

    } else if (name == "@PATIENT_INFO_02") {

    } else if (name == "@REGIST_INFO") {

    } else if (name == "@RESULT_CORNEA_CURVE") {

    } else if (name == "@RESULT_CORNEA_THICKNESS") {

    } else if (name == "@THUMBNAIL") {

    } else if (name == "@MAIN_MODULE_INFO") {

    } else {
      cerr << "Unexpected chunk name found : " << name << '\n';
    }
  }

}

ostream & operator << (ostream & out, fda::PInfo &p) {
    out << "model_name : " << p.model_name << '\n';
    out << "serial_name : " << p.serial_no << '\n';
    out << "version : " << p.version << '\n';
    out << "date : " << p.year << "/" << p.month << "/" << p.day << ' ';
    out << p.hour << ":" << p.minute << ":" << p.second << '\n';
}
