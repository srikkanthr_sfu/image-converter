#include <algorithm>
#include <cstdio>
#include <cstring>
#include <iomanip>
#include <iterator>
#include <sstream>
#include <fstream>
#include <stdexcept>
#include <etwoe.cpp>
#include <map>
#include <iostream>
#include "nifti1_io.h"
#include <fda.hpp>
#include <zlib.h>

using namespace std;

void nifti_from_E2E(nifti_image *&nii, etwoe::File &f) {
	nii->ndim = 3;
	nii->dim[2] = f.tomogram.size();
	if (!f.tomogram.empty()) nii->dim[1] = f.tomogram[0].size();
	if (!f.tomogram[0].empty()) nii->dim[0] = f.tomogram[0][0].size();
	nii->nx = nii->dim[0];
	nii->ny = nii->dim[1];
	nii->nz = nii->dim[2];
	int nx = nii->nx, ny = nii->ny, nz = nii->nz;
	double *ptr = new double[nx * ny * nz];
	double *beg = ptr;
	for (int i=0;i<nz;i++) for (int j=0;j<ny;j++) for (int k=0;k<nx;k++) {
		*ptr++ = f.tomogram[i][j][k];
	}
	nii->data = (void*) beg;
	nii->nvox = nx * ny * nz;
	nii->dx  = nii->dy = nii->dz = 1;
}

void nifti_from_FDA(nifti_image *&nii, fda::File &f) {
	nii->ndim = 3;
	nii->dim[2] = f.img.size();
	if (!f.img.empty()) nii->dim[1] = f.img[0].size();
	if (!f.img[0].empty()) nii->dim[0] = f.img[0][0].size();

	nii->nx = nii->dim[0];
	nii->ny = nii->dim[1];
	nii->nz = nii->dim[2];
	int nx = nii->nx, ny = nii->ny, nz = nii->nz;
	double *ptr = new double[nx * ny * nz];
	double *beg = ptr;
	for (int i=0;i<nz;i++) for (int j=0;j<ny;j++) for (int k=0;k<nx;k++) {
		*ptr++ = f.img[i][j][k];
	}
	nii->data = (void*) beg;
	nii->nvox = nx * ny * nz;
	nii->dx  = nii->dy = nii->dz = 1;
}

map<string, int> valid_file_types;
map<string, int> options;

/* File type 
1 -> e2e
2 -> fda
3 -> nii
*/

int chk_and_get_type(string str) {
	int n = str.length();
	if (n < 5) throw(runtime_error("Invalid file name\n"));
	string res = str.substr(n-3, 3);
	if (valid_file_types.find(res) == valid_file_types.end()) throw(runtime_error("Invalid file type, not supported\n"));
	return valid_file_types[res];
}

void show_help() {
	cout << "Commands and their Usage : \n";
	cout << "-n (or) --nii  : output nii equivalent of input file\n";
	cout << "-f (or) --fundus : output fundus image as text file\n";
	cout << "-p (or) --patientinfo : output patient info as text file\n";
	cout << "Specify output file destination in each of the above cases immediately after the option\n";
	cout << "Note : If file with same name is already present, it is overwritten in case of fundus and ";
	cout<< "patientinfo, however it is not overwritten in case of nii\n";
}

void setup() {
	// setup valid file types with their id's
	valid_file_types.insert({"nii",3});
	valid_file_types.insert({"e2e",1});
	valid_file_types.insert({"E2E",1});
	valid_file_types.insert({"fda",2});
	valid_file_types.insert({"fds",2});

	options.insert({"--nii", 1});
	options.insert({"-n", 1});
	options.insert({"-f", 2});
	options.insert({"--fundus", 2});
	options.insert({"-p", 3});
	options.insert({"--patientinfo", 3});
}

void ThrowCmdError() {
	show_help();
	runtime_error("Invalid command\n");
	exit(1);
}

int main(int argc, char* argv[]) {
	setup();
	// fstream f("/home/sramacha/Documents/temp/RawImage.bin", ios::in | ios::binary);
	if (argc < 2) ThrowCmdError();
	bool DEBUG = false;
	for (int i=2;i<argc;i++) {
		if (string(argv[i]) == "--dbg") DEBUG = true;
	}
	if (!DEBUG) freopen("/dev/null", "w", stderr);

	string src_file(argv[1]);

	int src_type = chk_and_get_type(src_file);

	etwoe::File fe;
	fda::File ff;
	nifti_image *nii;

	if (src_type == 1) {
		etwoe::read(src_file.c_str(), fe);
	} else if (src_type == 2) {
		ff.read(src_file.c_str());	
	} else ThrowCmdError();	


	for (int i=2;i<argc;i++) {
		auto it = options.find(string(argv[i]));
		if (it == options.end()) continue;
		else if (it->second == 1) {
			if (i+1 == argc) ThrowCmdError();
			string dest_file(argv[i+1]);
			i++;
			nifti_image *nii = new nifti_image;
			// memset(nii, 0, sizeof(nifti_image));
			nii = nifti_image_read("../sample/testwrite_nii.nii", 1);
			if (src_type == 1) {
				nifti_from_E2E(nii, fe);
			} else if (src_type == 2) {
				nifti_from_FDA(nii, ff);
			}
			nifti_set_filenames(nii, dest_file.c_str(), 1, 1);
			nifti_image_write(nii);
			cerr << "Nifti image written successfully\n";
		} else if (it->second == 2) {
			if (i+1 == argc) ThrowCmdError();
			string dest_file(argv[i+1]);
			i++;
			if (src_type == 1) {
				fe.PutFundusRaw(dest_file.c_str());
			} else if (src_type == 2) {
				ff.PutFundusRaw(dest_file.c_str());
			}
			cerr << "Fundus image extracted successfully\n";
		} else if (it->second == 3) {
			if (i+1 == argc) ThrowCmdError();
			string dest_file(argv[i+1]);
			i++;
			if (src_type == 1) {
				etwoe::OutputPatientInfo(dest_file.c_str(), fe);
			} else if (src_type == 2) {
				fstream fout(dest_file.c_str(), ios::out);
				fout << ff.p;
				fout.close();
			}
		}
	}
    return 0;
}
